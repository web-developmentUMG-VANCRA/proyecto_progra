import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AddvendedoraComponent } from './addvendedora.component';

describe('AddvendedoraComponent', () => {
  let component: AddvendedoraComponent;
  let fixture: ComponentFixture<AddvendedoraComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AddvendedoraComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AddvendedoraComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
