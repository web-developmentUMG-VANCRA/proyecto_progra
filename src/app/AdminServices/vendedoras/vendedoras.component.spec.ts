import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VendedorasComponent } from './vendedoras.component';

describe('VendedorasComponent', () => {
  let component: VendedorasComponent;
  let fixture: ComponentFixture<VendedorasComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VendedorasComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VendedorasComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
