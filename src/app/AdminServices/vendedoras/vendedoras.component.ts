import { Component, OnInit } from '@angular/core';
import { ProyectopService} from '../../service/proyectop.service'
import { Router } from '@angular/router';
import { MatDialog } from '@angular/material/dialog';
import { MyDialogComponent } from 'src/app/my-dialog/my-dialog.component';
@Component({
  selector: 'app-vendedoras',
  templateUrl: './vendedoras.component.html',
  styleUrls: ['./vendedoras.component.css']
})
export class VendedorasComponent implements OnInit {
  vendedoras:any=[];

  constructor(private proyectoService: ProyectopService, private router: Router, private dialog:MatDialog) { }

  ngOnInit() {
    this.getVendedoras();
  }

  getVendedoras(){
    this.proyectoService.getVendedoras().subscribe(
      res => { this.vendedoras=res},
      err =>console.error(err)
    )
  }

  deleteVendedora(id: string) {
    this.proyectoService.deleteVendedora(id)
      .subscribe(
        res => {
          console.log(res);
          this.getVendedoras();
        },
        err => console.error(err)
      )
      this.proyectoService.dialogT=2;
    this.openDialog(id);
  }
  
  openDialog(produc:any): void {
    this.dialog.open(MyDialogComponent, {
    });
  }
}
