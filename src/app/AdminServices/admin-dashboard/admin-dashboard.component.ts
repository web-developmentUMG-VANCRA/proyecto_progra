import { AfterViewInit, Component, OnDestroy, OnInit, ViewChildren, ViewChild, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { AuthenticationService } from 'src/app/service/authentication.service';
import { ProyectopService } from 'src/app/service/proyectop.service';
import { BaseChartDirective, } from 'ng2-charts';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit, OnDestroy, AfterViewInit {
  productos:any={};
  productos2:any=[];
  nombres:any=[];
  meses:any=[];
  mes=["01","02","03","04","05","06"];
  dbyMonth:any=[];

  @ViewChildren( BaseChartDirective )
  charts: QueryList<BaseChartDirective>
       

  public barChartOptions={
    scaleShowVerticalLines: false,
    responsive: true,
    scales: {
        yAxes: [{
          ticks: {
              min: 0,
              max: 25,
              stepSize: 5
          } }]}
  };

  public barChartLabels=[];
  public barChartType='bar';
  public barChartLegend=true;
  public barChartData=[];

  lineChartOptions={
    responsive: true,
    scales: {
        yAxes: [{
          ticks: {
              min: 0,
              max: 10,
              stepSize: 1
          } }]}
  };
  
  lineChartLabels=["Enero","Febrero","Marzo","Abril","Mayo","Junio"];
  lineChartType='line';
  lineChartLegend=true;
  lineChartData:any=[];
  

  constructor(private router: Router,private htpp:HttpClient,private loginservice: AuthenticationService,private proyectoService: ProyectopService) { }

  ngOnInit() {
    this.doNotificationSubscription();
    this.getProductos();
    this.getPedidos();
  }

  ngAfterViewInit(): void {
  }

  ngOnDestroy(): void {
  }

  async getProductos(){
    this.proyectoService.getProductos().subscribe(
      res => {
        this.chartUpdate(res);
        },
      err =>console.error(err)
    )
  }

  doNotificationSubscription(): void {
    try {
      this.proyectoService
        .getProductNotification()
        .subscribe((result) => {
          this.getProductos();
          this.getPedidos();
        });
    } catch (e) {
      console.log(e);
    }
  }

  async chartUpdate(res:any){
    this.productos2=res.map(function(a){return a.cantidad;});
    this.barChartData=[{data:this.productos2,label:'Productos disponibles',backgroundColor: [
      'rgba(255, 99, 132, 0.4)',
      'rgba(54, 162, 235, 0.4)',
      'rgba(255, 206, 86, 0.4)',
      'rgba(75, 192, 192, 0.4)',
      'rgba(153, 102, 255, 0.4)',
      'rgba(255, 159, 64, 0.4)'
  ],
  borderColor: [
      'rgba(255, 99, 132, 1)',
      'rgba(54, 162, 235, 1)',
      'rgba(255, 206, 86, 1)',
      'rgba(75, 192, 192, 1)',
      'rgba(153, 102, 255, 1)',
      'rgba(255, 159, 64, 1)'
  ],
  borderWidth: 1}];
    this.barChartLabels=res.map(function(a){return a.nombre;});
  }

  async getPedidos(){
    this.proyectoService.getPedidosA().subscribe(
      res=>{
        this.dbyMonth=[];
        this.getMeses(res);
      },
      err=>console.error(err)
    )
  }
  
  getMeses(res:any){
    this.mes.forEach(element => {
      this.meses=res.map(function(a){return a.fechae}).filter(ress => ress.includes("2020-"+ element));
      this.dbyMonth.push(this.meses.length);
    });
    console.log(this.dbyMonth);
    this.lineChartData= [{data: this.dbyMonth,
      label:'Pedidos por mes', 
      backgroundColor: ['rgba(13, 12, 12, 0.4)'], 
      borderColor: ['rgba(13, 12, 12, 0.9)'],
      pointBackgroundColor: ['rgba(13, 12, 12, 0.4)'], 
      pointBorderColor: ['rgba(13, 12, 12, 0.9)']}];
  }
}