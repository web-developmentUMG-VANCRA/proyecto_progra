import { Component, OnInit } from '@angular/core';
import { ProyectopService } from '../service/proyectop.service';

@Component({
  selector: 'app-cuenta',
  templateUrl: './cuenta.component.html',
  styleUrls: ['./cuenta.component.css']
})
export class CuentaComponent implements OnInit {
vendedora:any={
  telefonos:[]
}
  constructor(private vendedoraService:ProyectopService) { }

  ngOnInit() {
    let user=localStorage.getItem('username')
    console.log(user);
    this.vendedoraService.getVendedora(user).subscribe(
      res => {
        console.log(res);
        this.vendedora = res;
      },
      err => console.log(err)
    )
  }

}
