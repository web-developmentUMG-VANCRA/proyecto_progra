import { Component, OnInit } from '@angular/core';
import { ProyectopService } from '../service/proyectop.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-revisar',
  templateUrl: './revisar.component.html',
  styleUrls: ['./revisar.component.css']
})
export class RevisarComponent implements OnInit {
  carrito:any=[];
  total:number;
  pedido:any ={
    productospedidos:[]
  };
  producto:any=[];
  constructor(private proyectoService: ProyectopService,private route:Router) { }

  ngOnInit() {
    let user=localStorage.getItem('username')
    this.carrito=this.proyectoService.getCart();
    this.getCartT();
  }

  getCartT(){
    this.total=0;
    for(let i = 0; i < this.carrito.length; i++) {
      this.total += this.carrito[i].pcostot;
    }
  }

  agregarPedido(){
   this.setPedido();
   console.log(this.pedido);
    this.proyectoService.savePedido(this.pedido).subscribe(
      res => { 
        console.log(res);
        alert("¡Gracias por realizar tu pedido!");
        this.proyectoService.clearCart();
        this.route.navigate(['/home/cuenta/pedidos']);
      },
      err => console.error(err)
    )
  }

  setPedido(){
    let user=localStorage.getItem('username')
    this.pedido.vendedoracodigo=user;
    this.pedido.descripcion=this.total;
    this.configObject();
  }
  configObject(){
    for(let i = 0; i < this.carrito.length; i++) {
      this.pedido.productospedidos.push({
        productoid:this.carrito[i].productoid,
        cantidad:this.carrito[i].cantidad,
        pnombre:this.carrito[i].pnombre,
        pcosto:this.carrito[i].pcosto,
        pcostot:this.carrito[i].pcostot
      })
      this.obtainProduct(this.carrito[i].productoid,this.carrito[i].cantidad);
    }
  }
  
  obtainProduct(i,cant){
      this.proyectoService.getProducto(i)
      .subscribe(
        res => {
          this.producto = res;
          this.producto.cantidad=this.producto.cantidad-cant; 
          this.updateProduct(this.producto);
        },
        err => console.log(err)
        )
  }

  updateProduct(producto) {
    this.proyectoService.updateProducto(producto.idproductos, producto)
      .subscribe(
        res => { 
          console.log(res);
        },
        err => console.error(err)
      )
  }
}
